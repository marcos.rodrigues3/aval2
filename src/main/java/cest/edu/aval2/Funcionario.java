package cest.edu.aval2;

import java.math.BigDecimal;
import java.sql.Date;

public class Funcionario extends PessoaFisica{
	
	public BigDecimal getSalario() {
		return salario;
	}
	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}
	public int getMatriula() {
		return matriula;
	}
	public void setMatriula(int matriula) {
		this.matriula = matriula;
	}
	public String getClt() {
		return clt;
	}
	public void setClt(String clt) {
		this.clt = clt;
	}
	public String getFuncao() {
		return funcao;
	}
	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}
	public int getTempo_contribuicao() {
		return tempo_contribuicao;
	}
	public void setTempo_contribuicao(int tempo_contribuicao) {
		this.tempo_contribuicao = tempo_contribuicao;
	}
	
	private BigDecimal salario;
	private int matriula;
	private String clt;
	private String funcao;
	private int tempo_contribuicao;
	
	public Funcionario(String nome, String endereco,String genero,int idade, 
			Date nascimento, String cpf, String rg, BigDecimal salario) {
		
		super(nome, endereco, genero, idade, nascimento, cpf, rg);
		this.salario = salario;
	}
	
}
	
