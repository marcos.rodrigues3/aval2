package cest.edu.aval2;

public class Fornecedor extends IPessoa{
	
	public String getCategoria_produto() {
		return categoria_produto;
	}
	public void setCategoria_produto(String categoria_produto) {
		this.categoria_produto = categoria_produto;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	private String categoria_produto;
	private String sede;
	private String telefone;
	private String email;
	
	public Fornecedor (String nome, String endereco, String cnpj, String telefone, String email, String sede, String categoria_produto) {
		super(nome,endereco);
		this.categoria_produto = categoria_produto;	
        this.sede = sede;	
		this.telefone = telefone;
		this.email = email;
	}
		
}