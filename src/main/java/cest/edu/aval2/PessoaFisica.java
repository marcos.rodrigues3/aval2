package cest.edu.aval2;

import java.sql.Date;
 
public class PessoaFisica extends IPessoa{
	
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	private String cpf;
	private String rg;
	
		//constructor
		public PessoaFisica(String nome, String endereco,String genero,int idade, Date nascimento, String cpf, String rg) {
			super(nome,endereco,genero,idade,nascimento);
			this.cpf = cpf;
			this.rg = rg;
		}

}